package dynamicObject.movement;


import java.util.Random;


public enum MoveDirection {
    UP, DOWN, LEFT, RIGHT;

    private static Random random;

    public static int getNextPosition(int currentPosition, int speed, MoveDirection moveDirection) {
        switch (moveDirection) {
            case DOWN:
            case RIGHT:
                return currentPosition + speed;
            case LEFT:
            case UP:
                return currentPosition - speed;
        }

        return -1;
    }

    public static MoveDirection getRandomDirection(GeneralDirection generalDirection) {
        double randomValue = Math.random();
        switch (generalDirection) {
            case LEFTRIGHT:
                if (randomValue < 0.5) {
                    return LEFT;
                }
                return RIGHT;
            case UPDOWN:
                if (randomValue < 0.5) {
                    return UP;
                }
                return DOWN;
        }

        return null;
    }
}
