#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "net/emcute.h"
#include "mqtt-lib.h"

#define NODE_NAME	"control"
#define NODE_IP		"fec0:affe::97"
#define MQTT_BROKER_IP	"fec0:affe::1"

static unsigned flags = EMCUTE_QOS_0; //bez potvrde primitka
static emcute_topic_t t; //elementi: const char *name; uint16_t id;
static emcute_topic_t t2; //elementi: const char *name; uint16_t id;
char *topic_pub = "control/CO2";
char *topic_pub2 = "control/CO";

static void on_pub (const emcute_topic_t *topic, void *data, size_t len)
{
	//dobiva se kazaljka na podatke
	//posiljatelj i primatelj moraju znati format tih podataka
 	int *in = (int *) data;
	int out = *in, rc;

	printf ("dobivena poruka za pretplatu '%s': %d\n",
		topic->name, (int) topic->id);

	if (len !=4){
		printf("Kriva duljina podataka");
		return;
	}
	

	if (strcmp(topic->name,"sensor/CO2")==0) {
		printf ("Senzor CO2 ocitao koncentraciju: %d ppm\n", *in);
		if (*in >2000){
			printf("Prijavljujem nedozvoljene razine CO2\n");
			rc = emcute_pub (&t, &out, sizeof(out), flags);
			if (rc != EMCUTE_OK) {
				printf ("error: greska prilikom objave na temu: %s\n", topic_pub);
			}
		} else {
			printf(" Prihvatljiva razina CO2\n");
		}
	} else if (strcmp(topic->name,"sensor/CO")==0){
		printf ("Senzor CO očitao koncentraciju: %d ppm\n", *in);
		if (*in >35){
			printf("Prijavljujem nedozvoljene razine CO\n");
			rc = emcute_pub (&t2, &out, sizeof(out), flags);
			if (rc != EMCUTE_OK) {
				printf ("error: greska prilikom objave na temu: %s\n", topic_pub2);
			}
		} else {
			printf(" Prihvatljiva razina CO\n");
		}
	}
	
}

int main (void)
{
	emcute_sub_t subscription;
	emcute_sub_t subscription2;
	char *topic_sub = "sensor/CO2";
	char *topic_sub2 = "sensor/CO";
	int rc;

	rc = mqtt_init (NODE_NAME, NODE_IP, MQTT_BROKER_IP);
	if (rc != 0) {
		printf ("error: greska prilikom inicijalizacije programa\n");
		return rc;
	}

	
	t.name = topic_pub;
	rc = emcute_reg (&t); //dodjela id-a za zadano ime - od brokera
	if (rc != EMCUTE_OK) {
		printf ("error: greska prilikom dohvacanja topic ID-a\n");
		return 1;
	}
	t2.name = topic_pub2;
	rc = emcute_reg (&t2); //dodjela id-a za zadano ime - od brokera
	if (rc != EMCUTE_OK) {
		printf ("error: greska prilikom dohvacanja topic ID-a\n");
		return 1;
	}

	subscription.topic.name = topic_sub; //na koji sadrzaj se pretplacuje
	subscription.cb = on_pub; //koju funkciju pozvati po primitku poruke
	rc = emcute_sub (&subscription, flags);
	if (rc != EMCUTE_OK) {
		printf ("error: greska prilikom pretplate na temu: %s\n", topic_sub);
		return 1;
	}
	

	subscription2.topic.name = topic_sub2; //na koji sadrzaj se pretplacuje
	subscription2.cb = on_pub; //koju funkciju pozvati po primitku poruke
	rc = emcute_sub (&subscription2, flags);
	if (rc != EMCUTE_OK) {
		printf ("error: greska prilikom pretplate na temu: %s\n", topic_sub2);
		return 1;
	}



	while (1) {
		pause();
	}

	return 0;
}
