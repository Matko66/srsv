package dynamicObject.abstractions;


import dynamicObject.movement.MoveDirection;


public abstract class DynamicObject implements IMovable {


    public int staticPosition, dynamicPosition, speed;
    public MoveDirection moveDirection;

    public DynamicObject(int staticPosition, int dynamicPosition, int speed, MoveDirection moveDirection) {
        this.staticPosition = staticPosition;
        this.dynamicPosition = dynamicPosition;
        this.speed = speed;
        this.moveDirection = moveDirection;
    }
}
