package model;

import java.util.ArrayList;
import java.util.List;

import object.Person;

public class SystemView {
    public SystemLift system = new SystemLift();
    private List<String> lines = new ArrayList<>();

    private void drawLayout() {
        lines.add("              Lift1     ");
        lines.add("Smjer/vrata:            ");
        lines.add("Stajanja:=====  **--  ==");
        lines.add("4:          |          |");
        lines.add("  ==========|          |");
        lines.add("3:          |          |");
        lines.add("  ==========|          |");
        lines.add("2:          |          |");
        lines.add("  ==========|          |");
        lines.add("1:          |          |");
        lines.add("========================");
    }

    public void drawSystem() {
        drawLayout();
        StringBuilder builderLift = new StringBuilder();

        lines.set(1, "Smjer/vrata: " + system.lift.getDirection() + "|| " + (system.lift.isOpenFlag() ? "O" : "Z"));

        builderLift.append("[");
        if (system.lift.peopleInLift != null) {
            synchronized (system.lift.peopleInLift) {
                for (Person p : system.lift.peopleInLift) {
                    builderLift.append(p.getSymbol());
                }
            }
        }
        builderLift.append("]");
        String stringLift = builderLift.toString();


        StringBuilder stringWaitingFloorOne = new StringBuilder("1:          |          |");

        if (system.floorOneWaiting != null) {
            synchronized (system.floorOneWaiting) {
                StringBuilder sequence = new StringBuilder();
                for (Person p : system.floorOneWaiting) {
                    sequence.append(p.getSymbol());
                }
                String seq = sequence.toString();
                stringWaitingFloorOne.replace(2, seq.length() + 2, seq);
            }
        }
        lines.set(9, stringWaitingFloorOne.toString());

        StringBuilder stringWaitingFloorTwo = new StringBuilder("2:          |          |");
        if (system.floorTwoWaiting != null) {
            synchronized (system.floorTwoWaiting) {
                StringBuilder sequence = new StringBuilder();
                for (Person p : system.floorTwoWaiting) {
                    sequence.append(p.getSymbol());
                }
                String seq = sequence.toString();
                stringWaitingFloorTwo.replace(2, seq.length() + 2, seq);
            }
        }
        lines.set(7, stringWaitingFloorTwo.toString());

        StringBuilder stringWaitingFloorThree = new StringBuilder("3:          |          |");
        if (system.floorThreeWaiting != null) {
            synchronized (system.floorThreeWaiting) {
                StringBuilder sequence = new StringBuilder();
                for (Person p : system.floorThreeWaiting) {
                    sequence.append(p.getSymbol());
                }
                String seq = sequence.toString();
                stringWaitingFloorThree.replace(2, seq.length() + 2, seq);
            }
        }
        lines.set(5, stringWaitingFloorThree.toString());

        StringBuilder stringWaitingFloorFour = new StringBuilder("4:          |          |");
        if (system.floorFourWaiting != null) {
            synchronized (system.floorFourWaiting) {
                StringBuilder sequence = new StringBuilder();
                for (Person p : system.floorFourWaiting) {
                    sequence.append(p.getSymbol());
                }
                String seq = sequence.toString();
                stringWaitingFloorFour.replace(2, seq.length() + 2, seq);
            }
        }
        lines.set(3, stringWaitingFloorFour.toString());

        String toChange;
        StringBuilder builder;
        synchronized (system.lift) {
            switch (system.lift.getPosition()) {
                case 0:
                    toChange = lines.get(9);
                    builder = new StringBuilder(toChange);
                    builder.replace(13, stringLift.length() + 13, stringLift);
                    lines.set(9, builder.toString());
                    break;
                case 1:
                    toChange = lines.get(8);
                    builder = new StringBuilder(toChange);
                    builder.replace(13, stringLift.length() + 13, stringLift);
                    lines.set(8, builder.toString());
                    break;
                case 2:
                    toChange = lines.get(7);
                    builder = new StringBuilder(toChange);
                    builder.replace(13, stringLift.length() + 13, stringLift);
                    lines.set(7, builder.toString());
                    break;
                case 3:
                    toChange = lines.get(6);
                    builder = new StringBuilder(toChange);
                    builder.replace(13, stringLift.length() + 13, stringLift);
                    lines.set(6, builder.toString());
                    break;
                case 4:
                    toChange = lines.get(5);
                    builder = new StringBuilder(toChange);
                    builder.replace(13, stringLift.length() + 13, stringLift);
                    lines.set(5, builder.toString());
                    break;
                case 5:
                    toChange = lines.get(4);
                    builder = new StringBuilder(toChange);
                    builder.replace(13, stringLift.length() + 13, stringLift);
                    lines.set(4, builder.toString());
                    break;
                case 6:
                    toChange = lines.get(3);
                    builder = new StringBuilder(toChange);
                    builder.replace(13, stringLift.length() + 13, stringLift);
                    lines.set(3, builder.toString());
                    break;
                default:
                    break;
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.join("\n", lines));
        java.lang.System.out.println(stringBuilder.toString());
        System.out.println("\n" + "requests: " + system.requests.toString() + "\n" );

        lines.clear();
        clearScreen();


    }

    private void clearScreen() {
        for (int i = 0; i < 2; i++) {
            java.lang.System.out.println();
        }
    }
}
