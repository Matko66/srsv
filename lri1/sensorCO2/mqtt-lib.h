#include <stdio.h>
#include <stdlib.h>

#include "net/emcute.h" //https://doc.riot-os.org/group__net__emcute.html

#define EMCUTE_PORT (1885U)
#define EMCUTE_PRIO (THREAD_PRIORITY_MAIN - 1)

static char stack[THREAD_STACKSIZE_DEFAULT];

static void *emcute_thread(void *arg)
{
	emcute_run(EMCUTE_PORT, (char*) arg);
	return NULL;
}

static int emcute_connect(char *gw_addr)
{
	sock_udp_ep_t gw = {.family = AF_INET6, .port = EMCUTE_PORT};
	int rc;

	if (ipv6_addr_from_str((ipv6_addr_t *)&gw.addr.ipv6, gw_addr) == NULL) {
		printf("error: greska prilikom parsisranja IPv6 adrese\n");
		return 1;
	}

	rc = emcute_con(&gw, true, NULL, NULL, 0, 0);
	if (rc != EMCUTE_OK) {
		return 1;
	}

	return 0;
}


static int set_ipv6_addr(char* addr_str) {
	int rc;
	kernel_pid_t iface = 5;
	ipv6_addr_t addr;
	uint16_t flags = GNRC_NETIF_IPV6_ADDRS_FLAGS_STATE_VALID;
	uint8_t prefix_len;

	if (ipv6_addr_from_str(&addr, addr_str) == NULL) {
		printf("error: greska prilikom parsisranja IPv6 adrese\n");
		return 1;
	}

	prefix_len = ipv6_addr_split_int(addr_str, '/', 64U);
	if (prefix_len < 1) {
		prefix_len = 64U;
	}

	flags |= (prefix_len << 8U);

	// Dodjeljivanje staticke adrese sucelju
	rc = gnrc_netapi_set(iface, NETOPT_IPV6_ADDR, flags, &addr, sizeof(addr));
	if (rc < 0) {
		printf("error: greska prilikom dodjelivanja IPv6 adrese\n");
		return 1;
	}

	return 0;
}

int mqtt_init(char *mcute_id, char *local_addr, char *serv_addr) {
	int rc;

	// Stvaranje posebne dretve za emcute bibiloteku
	thread_create(stack, sizeof(stack), EMCUTE_PRIO, 0, emcute_thread, mcute_id, "emcute");

	// Postavljanje IPv6 adrese virtualnog sucelja dodijeljenog programu
	rc = set_ipv6_addr(local_addr);
	if (rc != 0) {
		printf("error: greska prilikom postavljanj IPv6 adrese\n");
		return 1;
	}

	rc = emcute_connect(serv_addr);
	if (rc != 0) {
		printf("greska prilikom spajanja na MQTT broker\n");
		return 1;
	}

	return 0;
}
