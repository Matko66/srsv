package model;

import object.Person;
import object.Lift;

import java.util.ArrayList;

public class SystemLift {
    public ArrayList<Person> floorOneWaiting= new ArrayList<>();
    public ArrayList<Person> floorTwoWaiting= new ArrayList<>();
    public ArrayList<Person> floorThreeWaiting= new ArrayList<>();
    public ArrayList<Person> floorFourWaiting= new ArrayList<>();

    public Lift lift=new Lift(0);
    public ArrayList<Integer> requests= new ArrayList<>();

    public static final int floorOneIndex = 0;
    public static final int floorTwoIndex = 2;
    public static final int floorThreeIndex = 4;
    public static final int floorFourIndex = 6;
}
