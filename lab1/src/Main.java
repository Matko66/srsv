import cli.CLIView;
import dynamicObject.movement.GeneralDirection;
import dynamicObject.movement.MoveDirection;
import model.CrossRoadModel;

import java.util.*;

import dynamicObject.impl.Vehicle;

public class Main {
    public static void main(String[] args) {
        CLIView cli = CLIView.shared;
        CrossRoadModel model = CLIView.model;

        Thread moveVehicles = new Thread() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(300);
                    } catch (Exception e) {

                    }

                    // move vehicles in RIGHT direction
                    List<Vehicle> foundRight = new ArrayList<>();
                    synchronized (model.right) {
                        for (Vehicle v : model.right) {
                            if (model.canVehicleMove(v)) {
                                v.makeStep();
                            }
                            if (v.dynamicPosition > CLIView.KRAJ) {
                                foundRight.add(v);
                            }
                        }
                        model.right.removeAll(foundRight);
                    }

                    // move vehicles in LEFT direction
                    List<Vehicle> foundLeft = new ArrayList<>();
                    synchronized (model.left) {
                        for (Vehicle v : model.left) {
                            if (model.canVehicleMove(v)) {
                                v.makeStep();
                            }
                            if (v.dynamicPosition < 0) {
                                foundLeft.add(v);
                            }
                        }
                        model.left.removeAll(foundLeft);
                    }

                    //move pedestrians in DOWN direction
                    List<Vehicle> foundDown = new ArrayList<>();
                    synchronized (model.down) {
                        for (Vehicle p : model.down) {
                            if (model.canPedestrianMove(p)) {
                                p.makeStep();
                            }
                            if (p.dynamicPosition > CLIView.KRAJ_ZEBRE) {
                                foundDown.add(p);
                            }
                        }
                        model.down.removeAll(foundDown);
                    }

                    //move pedestrians in UP direction
                    List<Vehicle> foundUp = new ArrayList<>();
                    synchronized (model.up) {
                        for (Vehicle p : model.up) {
                            if (model.canPedestrianMove(p)) {
                                p.makeStep();
                            }
                            if (p.dynamicPosition < 0) {
                                foundUp.add(p);
                            }
                        }
                        model.up.removeAll(foundUp);
                    }

                    cli.drawCrossRoad();
                }
            }
        };


        Thread createVehicle = new Thread() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {

                    }
                    MoveDirection moveDir = MoveDirection.getRandomDirection(GeneralDirection.LEFTRIGHT);
                    int length;
                    int random = getRandomNumberInRange(0, 100);
                    if (random < 5) {
                        random = getRandomNumberInRange(1, 9);
                        length = 2;
                    } else if (random >= 5 && random < 85) {
                        random = getRandomNumberInRange(10, 50);
                        length = 3;
                    } else if (random >= 85 && random < 95) {
                        random = getRandomNumberInRange(51, 60);
                        length = 4;
                    } else {
                        random = getRandomNumberInRange(61, 70);
                        length = 5;
                    }

                    if (moveDir.equals(MoveDirection.RIGHT)) {

                        String id = Integer.toString(random);
                        boolean flag = true;
                        synchronized (model.right) {
                            for (Vehicle vehicle : model.right) {
                                if (vehicle.id.equals(id) || vehicle.dynamicPosition == 0) {
                                    flag = false;
                                }
                            }
                            for (Vehicle vehicle : model.left) {
                                if (vehicle.id.equals(id)) {
                                    flag = false;
                                }
                            }
                            if (flag) {
                                model.right.add(new Vehicle(3, 0, 3, moveDir, id, length));
                            } else continue;
                        }
                    } else if (moveDir.equals(MoveDirection.LEFT)) {

                        String id = Integer.toString(random);
                        boolean flag = true;
                        synchronized (model.left) {
                            for (Vehicle vehicle : model.left) {
                                if (vehicle.id.equals(id) || vehicle.dynamicPosition == 44) {
                                    flag = false;
                                }
                            }
                            for (Vehicle vehicle : model.right) {
                                if (vehicle.id.equals(id)) {
                                    flag = false;
                                }
                            }
                            if (flag) {
                                model.left.add(new Vehicle(5, 44, 3, moveDir, id, length));
                            } else continue;
                        }
                    }
                }
            }
        };

        Thread createPedestrian = new Thread() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10000);
                    } catch (Exception e) {

                    }
                    MoveDirection moveDir = MoveDirection.getRandomDirection(GeneralDirection.UPDOWN);


                    if (moveDir.equals(MoveDirection.UP)) {
                        int random = getRandomNumberInRange(65, 90);
                        char c = (char) random;
                        String id = Character.toString(c);
                        boolean flag = true;
                        synchronized (model.up) {
                            for (Vehicle pedestrian : model.up) {
                                if (pedestrian.id.equals(id) || pedestrian.dynamicPosition == 8) {
                                    flag = false;
                                }
                            }
                            if (flag) {
                                model.up.add(new Vehicle(23, 8, 1, moveDir, id, 1));
                            } else continue;
                        }
                    } else if (moveDir.equals(MoveDirection.DOWN)) {
                        int random = getRandomNumberInRange(97, 122);
                        char c = (char) random;
                        String id = Character.toString(c);
                        boolean flag = true;
                        synchronized (model.down) {
                            for (Vehicle pedestrian : model.down) {
                                if (pedestrian.id.equals(id) || pedestrian.dynamicPosition == 0) {
                                    flag = false;
                                }
                            }
                            if (flag) {
                                model.down.add(new Vehicle(21, 0, 1, moveDir, id, 1));
                            } else continue;
                        }
                    }

                }
            }

        };


        Thread trafficLights = new Thread() {
            public void run() {
                while (true) {
                    model.vehicleLight = CrossRoadModel.TrafficColor.RED;
                    model.vehicleLightFlag=0;
                    model.pedestrianLight = CrossRoadModel.TrafficColor.RED;


                    try {
                        Thread.sleep(5000);
                    } catch (Exception e) {
                    }

                    while (true) {
                        synchronized (model.vehicleLightFlag) {
                            if (model.vehicleLightFlag > 0) {
                                break;
                            }
                        }
                    }
                    model.vehicleLightFlag=0;
                    model.vehicleLight = CrossRoadModel.TrafficColor.GREEN;
                    try {
                        Thread.sleep(15000);
                    } catch (Exception e) {
                    }
                    model.vehicleLight = CrossRoadModel.TrafficColor.RED;
                    try {
                        Thread.sleep(5000);
                    } catch (Exception e) {
                    }

                    while (true){
                        synchronized (model.up){
                            synchronized (model.down){
                                if (model.up.size()==3){
                                    break;
                                } else if (model.down.size()==3){
                                    break;
                                }
                            }
                        }
                    }
                    model.pedestrianLight = CrossRoadModel.TrafficColor.GREEN;
                    try {
                        Thread.sleep(15000);
                    } catch (Exception e) {

                    }
                }

            }
        };


        trafficLights.start();
        createVehicle.start();
        createPedestrian.start();
        moveVehicles.start();

    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
