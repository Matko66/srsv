#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "net/emcute.h"
#include "mqtt-lib.h"

#define NODE_NAME	"subscriber"
#define NODE_IP		"fec0:affe::98"
#define MQTT_BROKER_IP	"fec0:affe::1"

static void on_pub (const emcute_topic_t *topic, void *data, size_t len)
{
	//dobiva se kazaljka na podatke
	//posiljatelj i primatelj moraju znati format tih podataka
 	int *in = (int *) data;
	

	if (len !=4){
		printf("Kriva duljina podataka");
		return;
	}

	printf ("dobivena poruka za pretplatu '%s': %d\n",
		topic->name, (int) topic->id);

	if ( strcmp(topic->name,"control/CO2")==0) {
		printf("Primljeno upozorenje o nedozvoljenoj koncentraciji CO2: %d ppm\n", *in);
	} else if (strcmp(topic->name,"control/CO")==0){
		printf("Primljeno upozorenje o nedozvoljenoj koncentraciji CO: %d ppm\n", *in);
	}


}

int main (void)
{
	unsigned int flags = EMCUTE_QOS_0; //bez potvrde primitka
	emcute_sub_t  sub_control;
	emcute_sub_t  sub_control2;
	char *topic_co = "control/CO";
	char *topic_co2 = "control/CO2";
	int rc;

	rc = mqtt_init (NODE_NAME, NODE_IP, MQTT_BROKER_IP);
	if (rc != 0) {
		printf ("error: greska prilikom inicijalizacije programa\n");
		return rc;
	}

	sub_control.topic.name = topic_co; //na koji sadrzaj se pretplacuje
	sub_control.cb = on_pub; //koju funkciju pozvati po primitku poruke
	rc = emcute_sub (&sub_control, flags);
	if (rc != EMCUTE_OK) {
		printf ("error: greska prilikom pretplate na temu: %s\n", topic_co);
		return 1;
	}

	sub_control2.topic.name = topic_co2; //na koji sadrzaj se pretplacuje
	sub_control2.cb = on_pub; //koju funkciju pozvati po primitku poruke
	rc = emcute_sub (&sub_control2, flags);
	if (rc != EMCUTE_OK) {
		printf ("error: greska prilikom pretplate na temu: %s\n", topic_co2);
		return 1;
	}

	
	while (1) {
		pause();
	}

	return 0;
}
