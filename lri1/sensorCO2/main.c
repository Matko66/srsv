#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "net/emcute.h"
#include "mqtt-lib.h"

#define NODE_NAME	"sensorCO2"
#define NODE_IP		"fec0:affe::99"
#define MQTT_BROKER_IP	"fec0:affe::1"
#define GORNJA_GRANICA	5000
#define DONJA_GRANICA	250

int main (void)
{
	srand(time(NULL));
	int num;

	unsigned flags = EMCUTE_QOS_0; //bez potvrde primitka
	emcute_topic_t t; //elementi: const char *name; uint16_t id;
	char *topic = "sensor/CO2";
	int rc;


	rc = mqtt_init (NODE_NAME, NODE_IP, MQTT_BROKER_IP);
	if (rc != 0) {
		printf ("error: greska prilikom inicijalizacije programa\n");
		return rc;
	}

	t.name = topic;
	rc = emcute_reg (&t); //dodjela id-a za zadano ime - od brokera
	if (rc != EMCUTE_OK) {
		printf ("error: greska prilikom dohvacanja topic ID-a");
		return 1;
	}

	while (1) {
		
		num = (rand() % (GORNJA_GRANICA-DONJA_GRANICA+1)) + DONJA_GRANICA;

		printf ("Ocitana koncentracija CO2 je: %d ppm\n", num);
		rc = emcute_pub (&t, &num, sizeof(num), flags);
		if (rc != EMCUTE_OK) {
			printf ("error: greska prilikom objave na temu: %s\n", topic);
		}
		sleep(5000);
	}

	return 0;
}
