package dynamicObject.abstractions;

public interface IMovable {
    void makeStep();
}
