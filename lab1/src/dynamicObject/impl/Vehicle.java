package dynamicObject.impl;

import dynamicObject.abstractions.DynamicObject;
import dynamicObject.movement.MoveDirection;

public class Vehicle extends DynamicObject {
    public String id;
    public String symbol;
    public int length;



    public Vehicle(int staticPosition, int dynamicPosition, int speed, MoveDirection moveDirection, String id, int length) {
        super(staticPosition, dynamicPosition, speed, moveDirection);
        this.id = id;
        this.length = length;
        switch (length) {
            case 1:
                this.symbol = id;
                break;
            case 2:
                if (moveDirection.equals(MoveDirection.RIGHT)) {
                    this.symbol = reverseString(id) + ">";
                } else {
                    this.symbol = "<" + id ;
                }
                break;
            case 3:
                if (moveDirection.equals(MoveDirection.RIGHT)) {
                    this.symbol = reverseString(id) + ">";
                } else {
                    this.symbol = "<" + id;
                }
                break;
            case 4:
                if (moveDirection.equals(MoveDirection.RIGHT)) {
                    this.symbol = reverseString(id) + "*>";
                } else {
                    this.symbol = "<*"+ id ;
                }
                break;
            case 5:
                if (moveDirection.equals(MoveDirection.RIGHT)) {
                    this.symbol = reverseString(id)+ "**>";
                } else {
                    this.symbol = "<**" + id;
                }
                break;
        }
    }

    @Override
    public void makeStep() {
        this.dynamicPosition = MoveDirection.getNextPosition(this.dynamicPosition, this.speed, this.moveDirection);
    }

    public static String reverseString (String s ){
        String reversed = new StringBuilder(s).reverse().toString();
        return reversed;
    }


}
