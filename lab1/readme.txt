# Sustavi za rad u stvarnom vremenu - laboratorijska vje�ba 1

GitLab repozitorij za laboratorijske vje�be u sklopu predmeta Sustavi za rad u stvarnom vremenu na 1. godini diplomskog studija Fakulteta elektrotehnike i ra�unarstva, 
modul Ra�unalno in�enjerstvo.

## Zadatak
Simulirati raskr��e i sudionike u njemu.
Neka semafor radi u ciklusima:

* 5 sekundi sve crveno
* 15 sekundi zeleno za aute i pje�ake u jednom smjeru (crveno ostalima)
* 5 sekundi sve crveno
* 15 sekundi zeleno za aute i pje�ake u drugom smjeru (crveno ostalima)

### Svojstva vozila su:

* stati�ka:brzina i duljina objekta
* dinami�ka: polo�aj (traka, polo�aj po�etka vozila)

## Okolina

* **IntelliJ IDEA** by JetBrains
* **Java JDK 1.8.0_191**

## Konfiguracija

* konfiguracijska datoteka nalazi se unutar projektnog direktorija pod nazivom *properties.json*

## Autori

* **Mario Matkovi�** - *student* - izrada vje�bi
* **Leonardo Jelenkovi�** - *profesor* - ocjenjivanje vje�bi


## Licenca

Ovaj projekt je vlasni�tvo Fakulteta elektrotehnike i ra�unarstva.