import model.SystemView;
import model.SystemLift;
import object.Direction;
import object.Person;

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> people = new ArrayList<>();
        SystemView systemView = new SystemView();
        SystemLift system = systemView.system;

        system.lift.setPosition(0);
        system.lift.setDirection(Direction.GORE);


        Thread moveLift = new Thread() {
            public void run() {
                while (true) {

                    system.lift.setOpenFlag(false);
                    int position = system.lift.getPosition();
                    system.lift.setCurrentFloor(position);
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                    }

                    boolean openDoors = false;
                    for (Person p : system.lift.peopleInLift) {
                        if (system.lift.getPosition() == p.getNextFloor()) openDoors = true;
                    }
                    synchronized (system.requests) {
                        Integer request = -1;
                        if (!system.requests.isEmpty()) {
                            request = system.requests.get(0);
                        }
                        if (system.lift.getCurrentFloor() == system.lift.getPosition() && (system.requests.contains(system.lift.getCurrentFloor()) || openDoors)) {
                            try {
                                Thread.sleep(1000);
                            } catch (Exception e) {
                            }
                            system.lift.setOpenFlag(true);
                            try {
                                Thread.sleep(1000);
                            } catch (Exception e) {
                            }
                            ArrayList<Person> toRemove = new ArrayList<>();
                            for (Person p : system.lift.peopleInLift) {
                                if (p.getNextFloor() == system.lift.getCurrentFloor()) {
                                    toRemove.add(p);
                                }
                            }
                            for (Person p : toRemove) {
                                system.lift.peopleInLift.remove(p);
                                synchronized (people) {
                                    people.remove(p.getSymbol());
                                }
                            }


                            try {
                                Thread.sleep(1000);
                            } catch (Exception e) {
                            }
                            switch (system.lift.getCurrentFloor()) {
                                case 0:
                                    synchronized (system.floorOneWaiting) {
                                        if (!system.floorOneWaiting.isEmpty()) {
                                            for (Person p : system.floorOneWaiting) {
                                                if (system.lift.peopleInLift.size() < 6) {
                                                    system.lift.peopleInLift.add(p);
                                                }
                                            }
                                            for (Person p : system.lift.peopleInLift) {
                                                system.floorOneWaiting.remove(p);
                                            }
                                        }
                                    }
                                    break;
                                case 2:
                                    synchronized (system.floorTwoWaiting) {
                                        if (!system.floorTwoWaiting.isEmpty()) {
                                            for (Person p : system.floorTwoWaiting) {
                                                if (system.lift.peopleInLift.size() < 6 && (p.getDirection() == system.lift.getDirection())) {
                                                    system.lift.peopleInLift.add(p);
                                                }
                                            }
                                            for (Person p : system.lift.peopleInLift) {
                                                system.floorTwoWaiting.remove(p);
                                            }
                                        }
                                    }
                                    break;
                                case 4:
                                    synchronized (system.floorThreeWaiting) {
                                        if (!system.floorThreeWaiting.isEmpty()) {
                                            for (Person p : system.floorThreeWaiting) {
                                                if (system.lift.peopleInLift.size() < 6 && (p.getDirection() == system.lift.getDirection())) {
                                                    system.lift.peopleInLift.add(p);
                                                }
                                            }
                                            for (Person p : system.lift.peopleInLift) {
                                                system.floorThreeWaiting.remove(p);
                                            }
                                        }
                                    }
                                    break;
                                case 6:
                                    synchronized (system.floorFourWaiting) {
                                        if (!system.floorFourWaiting.isEmpty()) {
                                            for (Person p : system.floorFourWaiting) {
                                                if (system.lift.peopleInLift.size() < 6) {
                                                    system.lift.peopleInLift.add(p);
                                                }
                                            }
                                            for (Person p : system.lift.peopleInLift) {
                                                system.floorFourWaiting.remove(p);
                                            }
                                        }
                                    }
                                    break;

                            }
                            try {
                                Thread.sleep(1000);
                            } catch (Exception e) {
                            }

                            system.lift.setOpenFlag(false);

                        }
                        if (!system.requests.isEmpty() && system.requests.contains(request)) {
                            switch (request) {
                                case 0:
                                    if (system.floorOneWaiting.isEmpty()) system.requests.remove(request);
                                    break;
                                case 2:
                                    if (system.floorTwoWaiting.isEmpty()) system.requests.remove(request);
                                    break;
                                case 4:
                                    if (system.floorThreeWaiting.isEmpty()) system.requests.remove(request);
                                    break;
                                case 6:
                                    if (system.floorFourWaiting.isEmpty()) system.requests.remove(request);
                            }
                            //                          if (request == system.lift.getCurrentFloor()) system.requests.remove(0);
                        }
                    }
                    synchronized (system.lift) {
                        if (system.lift.peopleInLift.isEmpty() ) {
                            synchronized (system.requests) {
                                if (!system.requests.isEmpty()) {
                                    int requestFloor = system.requests.get(0);
                                    if (requestFloor == system.lift.getCurrentFloor()) {
                                        if (requestFloor == 6) system.lift.setDirection(Direction.DOLJE);
                                        if (requestFloor == 0) system.lift.setDirection(Direction.GORE);
                                    } else {
                                        if (requestFloor > system.lift.getCurrentFloor()) {
                                            system.lift.setDirection(Direction.GORE);
                                        } else {
                                            system.lift.setDirection(Direction.DOLJE);
                                        }
                                    }
                                }
                            }
                        } else {
                            if (system.lift.peopleInLift.get(0).isForgot()) {
                                Person forgetfulPerson = system.lift.peopleInLift.get(0);
                                system.lift.peopleInLift.remove(0);
                                system.lift.peopleInLift.add(forgetfulPerson);
                                System.out.println("Putnik nije pritisnuo tipku u dizalu");
                                if (system.lift.peopleInLift.size()==1) {
                                    synchronized (system.requests) {
                                        if (!system.requests.isEmpty()) {
                                            int requestFloor = system.requests.get(0);
                                            if (requestFloor == system.lift.getCurrentFloor()) {
                                                if (requestFloor == 6) system.lift.setDirection(Direction.DOLJE);
                                                if (requestFloor == 0) system.lift.setDirection(Direction.GORE);
                                            } else {
                                                if (requestFloor > system.lift.getCurrentFloor()) {
                                                    system.lift.setDirection(Direction.GORE);
                                                } else {
                                                    system.lift.setDirection(Direction.DOLJE);
                                                }
                                            }
                                        }
                                    }
                                }


                            } else {
                                int nextFloor = system.lift.peopleInLift.get(0).getNextFloor();
                                if (nextFloor == system.lift.getCurrentFloor()) {
                                    continue;
                                } else if (nextFloor > system.lift.getCurrentFloor()) {
                                    system.lift.setDirection(Direction.GORE);
                                } else {
                                    system.lift.setDirection(Direction.DOLJE);
                                }
                            }

                        }
                        int pos = system.lift.getPosition();
                        if (system.lift.peopleInLift.isEmpty() && system.requests.isEmpty()) continue;
                        if (system.lift.getDirection() == Direction.DOLJE) {
                            if (pos != 0) system.lift.setPosition(pos - 1);
                        } else {
                            if (pos != 6) system.lift.setPosition(pos + 1);
                        }
                    }


                }
            }

        };

        Thread createPerson = new Thread() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(5000);
                    } catch (Exception e) {
                    }

                    int random = getRandomNumberInRange(65, 90);
                    char c = (char) random;
                    String symbol = Character.toString(c);
                    if (people.contains(symbol)) {
                        continue;
                    }
                    people.add(symbol);
                    System.out.println(people);

                    int[] intArray = new int[]{0, 2, 4, 6};
                    int floor = getRandomNumberInRange(0, 3);
                    int nextFloor;
                    while (true) {
                        nextFloor = getRandomNumberInRange(0, 3);
                        if (nextFloor != floor) {
                            break;
                        }
                    }
                    int forget;
                    Person person = new Person(intArray[floor], intArray[nextFloor], symbol);
                    forget = getRandomNumberInRange(0, 100);
                    if (forget < 25) person.setForgot(true);

                    System.out.println(" createdfloor is :  " + intArray[floor] + "   nextfloor is : " + intArray[nextFloor] + "\n");

                    synchronized (system.requests) {
                        switch (person.getCreatedFloor()) {
                            case 0:
                                if (system.floorOneWaiting.size() < 10) {
                                    system.floorOneWaiting.add(person);
                                    forget = getRandomNumberInRange(0, 100);
                                    if (forget < 70) {
                                        if (!system.requests.contains(0)) system.requests.add(0);
                                    } else System.out.println("Zaboravio pritisnuti tipku\n");

                                }
                                break;
                            case 2:
                                if (system.floorTwoWaiting.size() < 10) {
                                    system.floorTwoWaiting.add(person);
                                    forget = getRandomNumberInRange(0, 100);
                                    if (forget < 70) {
                                        if (!system.requests.contains(2)) system.requests.add(2);
                                    } else System.out.println("Zaboravio pritisnuti tipku\n");
                                }
                                break;
                            case 4:
                                if (system.floorThreeWaiting.size() < 10) {
                                    system.floorThreeWaiting.add(person);
                                    forget = getRandomNumberInRange(0, 100);
                                    if (forget < 70) {
                                        if (!system.requests.contains(4)) system.requests.add(4);
                                    } else System.out.println("Zaboravio pritisnuti tipku\n");
                                }
                                break;
                            case 6:
                                if (system.floorFourWaiting.size() < 10) {
                                    system.floorFourWaiting.add(person);
                                    forget = getRandomNumberInRange(0, 100);
                                    if (forget < 70) {
                                        if (!system.requests.contains(6)) system.requests.add(6);
                                    } else System.out.println("Zaboravio pritisnuti tipku\n");
                                }
                                break;
                            default:
                                break;
                        }
                    }


                }
            }
        };


        Thread drawSystem = new Thread() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }
                    systemView.drawSystem();
                }
            }
        };

        drawSystem.start();
        createPerson.start();
        moveLift.start();
    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }


}
