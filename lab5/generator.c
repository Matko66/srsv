#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <sched.h>
#include <signal.h>
#include <mqueue.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>

#define NAZIV_REDA    "/MSGQ_SRSV_LAB5"
#define MAX_PORUKA_U_REDU    8
#define MAX_VELICINA_PORUKE 30
#define MAX_VELICINA_IME    20

struct dijeljeno {
    int id;
    int trajanje_obrade;
    int podaci[50];
};

struct dijeljeno_kljuc {
    pthread_mutex_t kljuc;
    int zadnji_id;
};


#define NAZIV_ZAJ_SPREMNIKA "/SRSV_LAB5"
#define VELICINA sizeof (struct dijeljeno)
#define VELICINA_KLJUCA sizeof (struct dijeljeno_kljuc)


int main(int argc, char *argv[]) {
    //shm_unlink(NAZIV_ZAJ_SPREMNIKA);
    //shm_unlink(NAZIV_REDA);
    int id, nacin_rasporedjivanja, broj_poslova, jedinice_vremena;

    struct dijeljeno *shared;
    struct dijeljeno_kljuc *shared_key;
    struct sched_param prioritet;
    mqd_t opisnik_reda;
    char poruka[MAX_VELICINA_PORUKE];
    char ime[MAX_VELICINA_IME];
    size_t duljina_poruke;
    unsigned prioritet_poruke=10;
    struct mq_attr attr;

    srand(time(NULL));

    /* Otvaranje reda poruka */
    attr.mq_flags=0;
    attr.mq_maxmsg = MAX_PORUKA_U_REDU;
    attr.mq_msgsize = MAX_VELICINA_PORUKE;
    opisnik_reda = mq_open (NAZIV_REDA, O_WRONLY | O_CREAT, 00600, &attr);
    if ( opisnik_reda == (mqd_t) -1) {
        perror("G: mq_open");
    }

    if (argc==3){
      broj_poslova = atoi(argv[1]);
      jedinice_vremena = atoi(argv[2]);
    }

    nacin_rasporedjivanja = SCHED_RR;
    /*Postavi nacin rasporedivanja i prioritet pocetnoj dretvi*/
    prioritet.sched_priority = 50;
    if (pthread_setschedparam(pthread_self(),
                              nacin_rasporedjivanja, &prioritet)) {
        perror("G: Greska: pthread_setschedparam (dozvole?)");
        exit(1);
    }


    /* stvori zajednicki spremnik */
    id = shm_open(NAZIV_ZAJ_SPREMNIKA, O_CREAT | O_RDWR, 00600);
    if (id == -1 || ftruncate(id, VELICINA_KLJUCA) == -1) {
        perror("G:shm_open/ftruncate");
        exit(1);
    }
    shared_key = mmap(NULL, VELICINA_KLJUCA, PROT_READ | PROT_WRITE, MAP_SHARED, id, 0);
    if (shared_key == (void *) -1) {
        perror("G:mmap");
        exit(1);
    }
    close(id);


    pthread_mutex_lock(&shared_key->kljuc);
    shared_key->zadnji_id+=broj_poslova;
    pthread_mutex_unlock(&shared_key->kljuc);


    for (int i = 0; i <broj_poslova; i++){
        int id_posla=i+shared_key->zadnji_id - broj_poslova;
        int trajanje_posla=rand() % jedinice_vremena + 1;

        sprintf(ime, "/SRSV_LAB5-%d", id_posla);


        /*Otvaranje vlastitog spremnika za svaki posao*/
        id = shm_open(ime, O_CREAT | O_RDWR, 00600);
        if (id == -1 || ftruncate(id, VELICINA) == -1) {
            perror("G: shm_open/ftruncate");
            exit(1);
        }
        shared = mmap(NULL, VELICINA, PROT_READ | PROT_WRITE, MAP_SHARED, id, 0);
        if (shared == (void *) -1) {
            perror("G: mmap");
            exit(1);
        }
        close(id);



        shared->id=id_posla;
        shared->trajanje_obrade=trajanje_posla;
        for (int j = 0; j<trajanje_posla; j++){
            shared->podaci[j]=rand()%1000+1;
        }


        printf("G: posao %d %d %s [", id_posla, trajanje_posla, ime);
        for (int j = 0; j<trajanje_posla; j++){
            printf("%d ", shared->podaci[j]);
        }
        printf("]\n");

        sprintf(poruka, "%d %d %s", id_posla, trajanje_posla, ime);
        poruka[MAX_VELICINA_PORUKE-1]='\0';
        //duljina_poruke=strlen(poruka);
        if ( mq_send (opisnik_reda, poruka, MAX_VELICINA_PORUKE, prioritet_poruke)){
            perror("G: mq_send");
        }



        sleep(1);
    }
    return 0;
}
