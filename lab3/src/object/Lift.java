package object;

import object.Direction;

import java.util.ArrayList;

public class Lift {
    private int currentFloor;
  //  private int nextFloor;
    private Direction direction;
    private boolean openFlag;
    private int position;
    public ArrayList<Person> peopleInLift;

    public Lift(int currentFloor) {
        this.currentFloor = currentFloor;
        openFlag = false;
        peopleInLift=new ArrayList<>();
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean isOpenFlag() {
        return openFlag;
    }

    public void setOpenFlag(boolean openFlag) {
        this.openFlag = openFlag;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


}
