package cli;

import model.CrossRoadModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import dynamicObject.impl.Vehicle;

public class CLIView {
    public static final int KRAJ = 44;
    public static final int KRAJ_ZEBRE = 8;
    public static CLIView shared = new CLIView();
    public static CrossRoadModel model = new CrossRoadModel();
    private List<String> roadLines = new ArrayList<>();
    private StringBuilder upString;

    private void drawRoadLines() {
        roadLines.add("                                            ");
        roadLines.add("                                            ");
        roadLines.add("==================== - - ====================");
        roadLines.add("                     - -                     ");
        roadLines.add("- - - - - - - - - - - - - - - - - - - - - - ");
        roadLines.add("                     - -                     ");
        roadLines.add("==================== - - ====================");
        roadLines.add("                                            ");
        roadLines.add("                                            ");

    }

    public void drawCrossRoad() {
        drawRoadLines();
        // Some kind of string builder or maybe changing a copy of roadLines list must be used for inserting objects into correct field
        StringBuilder stringBuilder = new StringBuilder();

        //Set roadLine for vehicles moving in RIGHT direction
        StringBuilder rightString = new StringBuilder("                     - -                     ");
        for (Vehicle v : model.right) {
            String symbol = v.symbol;
            int length = v.length;
            if (v.length > v.dynamicPosition + 1) {
                symbol = v.symbol.substring(v.length - v.dynamicPosition - 1, v.length );
                length = v.dynamicPosition + 1;
            }
            rightString.replace(v.dynamicPosition - length + 1, v.dynamicPosition+1, symbol);
        }
        roadLines.set(3, rightString.toString());


        //Set roadLine for vehicles moving in LEFT direction
        StringBuilder leftString = new StringBuilder("                     - -                     ");
        for (Vehicle v : model.left) {
            String symbol = v.symbol;
            int length = v.length;
            if (v.length > KRAJ - v.dynamicPosition + 1) {
                symbol = v.symbol.substring(0, KRAJ - v.dynamicPosition+1);
                length = KRAJ - v.dynamicPosition + 1;
            }
            leftString.replace(v.dynamicPosition, v.dynamicPosition + length , symbol);
        }
        roadLines.set(5, leftString.toString());

        //Set roadLines for pedestrians moving in DOWN direction
        StringBuilder downString= new StringBuilder();
        for (Vehicle p : model.down){
            downString.append(roadLines.get(p.dynamicPosition));
            downString.replace(p.staticPosition, p.staticPosition+1, p.id);
            roadLines.set(p.dynamicPosition, downString.toString());
            downString=new StringBuilder();
        }

        //Set roadlines for pedestrians moving in UP direction
        StringBuilder upString= new StringBuilder();
        for (Vehicle p : model.up){
            upString.append(roadLines.get(p.dynamicPosition));
            upString.replace(p.staticPosition, p.staticPosition+1, p.id);
            roadLines.set(p.dynamicPosition, upString.toString());
            upString=new StringBuilder();
        }

        stringBuilder.append(String.join("\n", roadLines));
        System.out.println(stringBuilder.toString());
        roadLines.clear();
        clearScreen();

    }


    private void clearScreen() {
        for (int i = 0; i < 2; i++) {
            System.out.println();
        }
    }
}
