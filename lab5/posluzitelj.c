#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <sched.h>
#include <signal.h>
#include <mqueue.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdbool.h>

#define NAZIV_REDA    "/MSGQ_SRSV_LAB5"
#define MAX_PORUKA_U_REDU    8
#define MAX_VELICINA_PORUKE 30
#define MAX_VELICINA_IME    20

struct dijeljeno {
    int id;
    int trajanje_obrade;
    int podaci[50];
};


#define NAZIV_ZAJ_SPREMNIKA "/SRSV_LAB5"
#define VELICINA sizeof (struct dijeljeno)
#define VELICINA_KLJUCA sizeof (struct dijeljeno_kljuc)

struct QNode {
    int id;
    int t;
    char ime[MAX_VELICINA_IME];
    struct QNode *next;
};

struct Queue {
    struct QNode *front, *rear;
};

struct QNode *newNode(int id, int t, char *ime) {
    struct QNode *temp = (struct QNode *) malloc(sizeof(struct QNode));
    temp->id = id;
    temp->t = t;
    strncpy(temp->ime, ime, MAX_VELICINA_IME);
    temp->ime[MAX_VELICINA_IME-1]='\0';
    return temp;
}

struct Queue *createQueue() {
    struct Queue *q = (struct Queue *) malloc(sizeof(struct Queue));
    q->front = q->rear = NULL;
    return q;
}

void enQueue(struct Queue *q, struct QNode* qnode) {
    struct QNode *temp = qnode;
    if (q->rear == NULL) {
        q->front = q->rear = temp;
        return;
    }
    q->rear->next = temp;
    q->rear = temp;

}

struct QNode *deQueue(struct Queue *q) {
    if (q->front == NULL) return NULL;
    struct QNode *temp = q->front;
    free(temp);

    q->front = q->front->next;

    if (q->front == NULL) {
        q->rear = NULL;
    }
    return temp;
}

struct Queue* queue;
static int broj_poslova;
unsigned long iteracije = 10000;
pthread_mutex_t monitor = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t imaPosla;
bool kraj = false;

static void *posao_radnika(void *param) {

    struct QNode *posao;
    struct dijeljeno *shared;

    int *Rid=param;
    int id;


    while (1) {
      pthread_mutex_lock(&monitor);
      if ( pthread_cond_wait(&imaPosla, &monitor) !=0){
        perror("R: cond wait error");
      }

        posao = deQueue(queue);

        if (posao != NULL){
            id = shm_open(posao->ime, O_CREAT | O_RDWR, 00600);
            if (id == -1 || ftruncate (id, VELICINA) == -1){
                perror("R: shm/open/ftruncate");
                exit(1);
            }
            shared=mmap ( NULL, VELICINA, PROT_READ | PROT_WRITE, MAP_SHARED, id, 0);
            if (shared == (void*) -1){
                perror("R: mmap");
                exit(1);
            }
            close(id);

            for(int i=0; i<shared->trajanje_obrade; i++){
                printf("R%d: id:%d obrada podatka: %d (%d/%d)\n", *Rid, shared->id,
                        shared->podaci[i], i+1, shared->trajanje_obrade );
                for (int j=0; j<iteracije; j++){
                    asm volatile ("":::"memory");
                }
            }
            munmap(shared, VELICINA);
            shm_unlink(NAZIV_ZAJ_SPREMNIKA);
        }
        else if(kraj){
            break;
        }
        else{
            printf("R%d: nema poslova, spavam", *Rid);
        }
        pthread_mutex_unlock(&monitor);
    }


    pthread_exit(NULL);
}

void obrada_signala(int sig, siginfo_t *info, void *context) {
    if (sig == SIGTERM) {
        kraj = true;
        printf("Kraj\n");
    }
}

int main(int argc, char *argv[]) {
    pthread_t *t;
    int broj_dretvi, cekanje, nacin_rasporedjivanja;
    struct sched_param prioritet;
    pthread_attr_t attr;
    double trajanje = 0.0;
    int timer=0;
    struct timespec requestStart, requestEnd;
    struct sigaction act;

    mqd_t opisnik_reda;
    char poruka[MAX_VELICINA_PORUKE];
    size_t duljina_poruke;
    unsigned prioritet_poruke;

    int id, vrijeme;
    char imeSpremnika[MAX_VELICINA_IME];
    int *threadId;

    if (argc==3){
      broj_dretvi = atoi(argv[1]);
      t = (pthread_t *) malloc(broj_dretvi * sizeof(pthread_t));
      cekanje = atoi(argv[2]);
    } else {
      perror("P: wrong arguments");
    }

    threadId=(int*)malloc(broj_dretvi*sizeof(int));
    queue = createQueue();

    /*Inicijalizacija monitora i reda uvjeta*/
  /*  if (pthread_mutex_init(&monitor, NULL) != 0) {
        printf("P: Monitor se nije uspio inicijalizirati\n");
        return 1;
    }*/
    if (pthread_cond_init(&imaPosla, NULL) != 0) {
        printf("P: inicijalizacija reda uvjeta imaPosla nije uspjelo\n");
        return 1;
    }

    nacin_rasporedjivanja = SCHED_RR;
    /*Postavi nacin rasporedivanja i prioritet pocetnoj dretvi*/
    prioritet.sched_priority = 60;
    if (pthread_setschedparam(pthread_self(),
                              nacin_rasporedjivanja, &prioritet)) {
        perror("P: pthread_setschedparam (dozvole?)");
        exit(1);
    }

    /*Postavke rasporedjivanja za nove dretve*/
    pthread_attr_init(&attr);
    pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attr, nacin_rasporedjivanja);
    prioritet.sched_priority = 40;
    pthread_attr_setschedparam(&attr, &prioritet);

    /* Postavi da se signal SIGTERM obradjuje funkcijom*/
    act.sa_sigaction = obrada_signala;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGTERM);
    act.sa_flags = SA_SIGINFO;
    sigaction(SIGTERM, &act, NULL);

    /*pronalazak potrebnog broja iteracija radnog cekanja*/
    while (trajanje < 1.0) {
        clock_gettime(CLOCK_MONOTONIC, &requestStart);
        for (int i = 0; i < iteracije; ++i) {
            asm volatile ("":: :"memory");
        }
        clock_gettime(CLOCK_MONOTONIC, &requestEnd);
        trajanje = (double) (requestEnd.tv_sec - requestStart.tv_sec) +
                   (double) (requestEnd.tv_nsec - requestStart.tv_nsec) / 1000000000.0;
        iteracije *= 2;
    }
    iteracije = iteracije / trajanje;

    /*stvaranje dretvi radnika */
    for (int i = 0; i < broj_dretvi; i++) {
        threadId[i]=i+1;
        if (pthread_create(&t[i], &attr, posao_radnika, (void *) &threadId[i])) {
            perror("P: pthread_create");
            return 1;
        }
    }

    /* zaprimanje poruka iz reda */
    opisnik_reda = mq_open(NAZIV_REDA, O_RDONLY);
    if (opisnik_reda == (mqd_t) - 1) {
        perror("posluzitelj:mq_open");
        return -1;
    }


    printf("P: pokrecem zaostale poslove (nakon isteka vise od %d sekundi)\n", cekanje);
    trajanje = 0.0;

    while (1) {
        pthread_mutex_lock(&monitor);
        if (clock_gettime(CLOCK_MONOTONIC, &requestStart) ==-1){
            perror("clock gettime");
            exit(EXIT_FAILURE);
        }

        duljina_poruke = mq_receive(opisnik_reda, poruka, MAX_VELICINA_PORUKE,
                                    &prioritet_poruke);

        if (duljina_poruke < 0) {
            perror("P: mq_receive");
        } else /*if (strstr(poruka, "/SRSV_LAB5") != NULL)*/ {
            poruka[MAX_VELICINA_PORUKE-1]='\0';
            printf("P: zaprimio %s \n", poruka);
            sscanf(poruka, "%d %d %s", &id, &vrijeme, imeSpremnika);


            struct QNode* node = newNode(id, vrijeme, imeSpremnika);
            enQueue(queue, node);

            broj_poslova++;

        }
        clock_gettime(CLOCK_MONOTONIC, &requestEnd);
        //trajanje += (double) (requestEnd.tv_sec - requestStart.tv_sec) +
        //            (double) (requestEnd.tv_nsec - requestStart.tv_nsec) / 1000000000.0;


        if (timer >= cekanje || broj_poslova >= 1) {
            if (pthread_cond_broadcast(&imaPosla) != 0){
              perror("P: broadcast error");
            }
            timer=0;
            //pthread_cond_signal(&imaPosla);
        }
        pthread_mutex_unlock(&monitor);
        timer= timer+1;
        sleep(1);
        if (kraj) break;
    }

    return 0;
}
