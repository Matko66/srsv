﻿# Sustavi za rad u stvarnom vremenu - Laboratorij računalnog inženjerstva 1

## Zadatak

Osmisliti i simulirati neki sustav koji koristi:
* barem jednog brokera
* barem jednog čvora koji samo šalje podatke
* barem jednog čvora koji samo prima podatke (preko pretplate brokeru)
* barem jednog čvora koji i prima i šalje podatke (šalje po potrebi, nakon "obrade" primljenih).

Koristiti RIOT i MQTT (prethodno opisani) ili neke druge programe (bitno je samo da se sva komunikacija obavlja preko MQTT-a).
Svaki student treba osmisliti svoj sustav, ostvariti ga te opisati u kratkom tekstu (sa slikom).

## Opis sustava

	Kontrola koncentracije CO i CO2 u prostoriji
	
Zamišljeni sustav mjeri razine koncentracije CO (ugljikovog monoksida) i CO2 (ugljikovog dioksida) 
u prostoriji te zabilježava izmjere vrijednosti. Ako očitane razine koncentracija ovih plinova 
prelaze dopuštene, šalju se upozorenja na alarmni uređaj.

Dijelovi sustava:
	-senzor CO
	-senzor CO2
	-kontrola
	-alarm
	
shema: https://imgur.com/fYxtrnR 
	
	
## Autori

* **Mario Matkovi** - *student* - izrada vjebi
* **Leonardo Jelenkovi** - *profesor* - ocjenjivanje vjebi


## Licenca

Ovaj projekt je vlasnitvo Fakulteta elektrotehnike i raunarstva.