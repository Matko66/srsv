package model;

import cli.CLIView;
import dynamicObject.impl.Vehicle;
import dynamicObject.movement.MoveDirection;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CrossRoadModel {
    public enum TrafficColor {
        GREEN, RED
    }

    public static final int SEM_RIGHT = 20;
    public static final int SEM_LEFT = 24;
    public static final int SEM_UP = 5;
    public static final int SEM_DOWN = 3;


    public List<Vehicle> right = new ArrayList<>();
    public List<Vehicle> left = new ArrayList<>();
    public List<Vehicle> up = new ArrayList<>();
    public List<Vehicle> down = new ArrayList<>();
    public TrafficColor vehicleLight = TrafficColor.RED;
    public TrafficColor pedestrianLight = TrafficColor.RED;
    public Integer vehicleLightFlag;



    public boolean canVehicleMove(Vehicle vehicle) {

        if (vehicle.moveDirection == MoveDirection.RIGHT) {
            if (vehicleLight == TrafficColor.RED && vehicle.dynamicPosition < SEM_RIGHT && vehicle.dynamicPosition + vehicle.speed >= SEM_RIGHT) {
                return false;
            }


            Optional<Vehicle> vehicleInfront = right.stream().filter(v -> v.dynamicPosition > vehicle.dynamicPosition).min((v1, v2) -> v1.dynamicPosition - v2.dynamicPosition);
            if (vehicleInfront.isPresent()) {
                Vehicle v = vehicleInfront.get();
                int vehicleInFrontStart = v.dynamicPosition;
                int vehicleInFrontEnd = v.dynamicPosition - v.length + 1;

                int nextPossibleStart = vehicle.dynamicPosition + vehicle.speed;

                //if vehicle is at beginning and can't move turn on the flag for green light
                if (vehicle.dynamicPosition < vehicle.length && nextPossibleStart >= vehicleInFrontEnd && nextPossibleStart <= vehicleInFrontStart) {
                    this.vehicleLightFlag++;
                }
                return !(nextPossibleStart >= vehicleInFrontEnd && nextPossibleStart <= vehicleInFrontStart);
            } else return true;
        } else if (vehicle.moveDirection == MoveDirection.LEFT) {
            if (vehicleLight == TrafficColor.RED && vehicle.dynamicPosition > SEM_LEFT && vehicle.dynamicPosition - vehicle.speed <= SEM_LEFT) {
                return false;
            }

            Optional<Vehicle> vehicleInfront = left.stream().filter(v -> v.dynamicPosition < vehicle.dynamicPosition).min((v1, v2) -> v2.dynamicPosition - v1.dynamicPosition);
            if (vehicleInfront.isPresent()) {
                Vehicle v = vehicleInfront.get();
                int vehicleInFrontStart = v.dynamicPosition;
                int vehicleInFrontEnd = v.dynamicPosition + v.length - 1;

                int nextPossibleStart = vehicle.dynamicPosition - vehicle.speed;

                if (CLIView.KRAJ - v.dynamicPosition <= v.length && nextPossibleStart <= vehicleInFrontEnd ) {
                    this.vehicleLightFlag++;
                }
                return !(nextPossibleStart <= vehicleInFrontEnd && nextPossibleStart >= vehicleInFrontStart);
            } else return true;


        } else return false;

    }

    public boolean canPedestrianMove(Vehicle vehicle) {
        if (vehicle.moveDirection == MoveDirection.DOWN) {
            if (pedestrianLight == TrafficColor.RED && vehicle.dynamicPosition < SEM_DOWN && vehicle.dynamicPosition + vehicle.speed >= SEM_DOWN) {
                return false;
            }


            Optional<Vehicle> pedestrianInfront = down.stream().filter(v -> v.dynamicPosition > vehicle.dynamicPosition).min((v1, v2) -> v1.dynamicPosition - v2.dynamicPosition);
            if (pedestrianInfront.isPresent()) {
                Vehicle p = pedestrianInfront.get();
                int nextPossibleStep = vehicle.dynamicPosition + vehicle.speed;
                return !(nextPossibleStep >= p.dynamicPosition);
            } else return true;

        } else if (vehicle.moveDirection == MoveDirection.UP) {
            if (pedestrianLight == TrafficColor.RED && vehicle.dynamicPosition > SEM_UP && vehicle.dynamicPosition - vehicle.speed <= SEM_UP) {
                return false;
            }

            Optional<Vehicle> pedestrianInfront = up.stream().filter(v -> v.dynamicPosition < vehicle.dynamicPosition).min((v1, v2) -> v2.dynamicPosition - v1.dynamicPosition);
            if (pedestrianInfront.isPresent()) {
                Vehicle p = pedestrianInfront.get();
                int nextPossibleStep = vehicle.dynamicPosition - vehicle.speed;
                return !(nextPossibleStep <= p.dynamicPosition);
            } else return true;

        } else return false;

    }
}
