package object;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String symbol;
    private int createdFloor;
    private int nextFloor;
    private Direction direction;
    private boolean forgot=false;

    public boolean isForgot() {
        return forgot;
    }

    public void setForgot(boolean forgot) {
        this.forgot = forgot;
    }

    public Person(int createdFloor, int nextFloor, String symbol) {
        this.symbol=symbol;
        this.createdFloor = createdFloor;
        this.nextFloor = nextFloor;
        direction = nextFloor > createdFloor ? Direction.GORE : Direction.DOLJE;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getCreatedFloor() {
        return createdFloor;
    }

    public void setCreatedFloor(int createdFloor) {
        this.createdFloor = createdFloor;
    }

    public int getNextFloor() {
        return nextFloor;
    }

    public void setNextFloor(int nextFloor) {
        this.nextFloor = nextFloor;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
